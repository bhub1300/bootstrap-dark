#!/bin/sh

# CI_API_V4_URL="https://gitlab.com/api/v4"
# CI_PROJECT_ID="26550549"
mkdir public
cd public
for version in `curl "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages" | jq .[].version --raw-output`
do
  echo "Downloading ${version}"
  mkdir $version
  cd $version
  curl -O "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/cfmm_dark/${version}/select2-dark.min.css"
  curl -O "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/cfmm_dark/${version}/daterangepicker-dark.min.css"
  curl -O "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/cfmm_dark/${version}/bootstrap-datepicker-dark.min.css"
  curl -O "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/cfmm_dark/${version}/bootstrap-dark.min.css"
  cd ..
done
